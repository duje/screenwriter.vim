# screenwriter.vim

This Vim plugin allows you to convert written text into screenplay format. It automates the process of formatting your text to adhere to standard screenplay conventions.

## Installation

1. Clone this repository or download the `screenwriter.vim` file.

2. Move the `screenwriter.vim` file to your Vim plugin directory. For example, `~/.vim/plugin/` or `~/.config/nvim/plugin/` if you're using Neovim.

## Usage

1. Open your screenplay file in Vim or Neovim.

2. To convert the text to screenplay format, execute the following command:

`:call screenplay#convert()`

3. Follow the prompts to add a title page and choose paper size for PDF conversion.

4. If prompted, specify the screenplay title, author, and date for the title page.

5. After conversion, the screenplay will be displayed in the buffer with the proper formatting.

6. If you choose to convert to PDF, a PDF file will be generated using Pandoc with the specified paper size.

## Requirements

- Vim or Neovim (as text editor)
- Pandoc (for PDF conversion)

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please create an issue or submit a pull request.

## License

This plugin is licensed under the [Affero GPL](LICENSE).

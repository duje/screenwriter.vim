"screenwriter.vim

func! screenplay#convert()
  "Get the selected text or the entire buffer if no selection
  let text = ''
  if mode() == 'v'
    let [line_start, col_start] = getpos("'<")[1:2]
    let [line_end, col_end] = getpos("'>")[1:2]
    let text = join(getline(line_start, line_end), "\n")
  el
    let text = join(getline(1, line("$")), "\n")
  en

  "Define pattern variables
  let s_scene_heading = '\v^\s*(INT\.|EXT\.)'
  let s_character = '\v\((\w+)( \([a-zA-Z]+\))?\)'
  let s_parenthetical = '\v^([A-Z][A-Z\s]+)\n(\(.+\))'
  let s_dialogue = '\v^([A-Z][A-Z\s]+)\n(.+)'
  let s_transition = '\v^([A-Z][A-Z\s]+)'
  let s_action_description = '\v^\s*\zs([A-Za-z].+)'
  let s_page_break = '\v(\f\s*\n)'
  let s_continuation_dialogue = '\v^([A-Z][A-Z\s]+)\n\zs([^\t].+)'
  let s_dual_dialogue = '\v^([A-Z][A-Z\s]+)\n(.+)\n([A-Z][A-Z\s]+)\n(.+)'
  let s_scene_number = '\v^#(\d+)'

  "Perform the conversion logic here

  "Scene Heading
  let converted_text = substitute(text, s_scene_heading, '\n\n\0', 'g')

  "Scene Number
  let converted_text = substitute(converted_text, s_scene_number, '\n# \1', 'g')

  "Character
  let converted_text = substitute(converted_text, s_character, '\n\0', 'g')

  "Parenthetical
  let converted_text = substitute(converted_text, s_parenthetical, '\1\n\t\2', 'g')

  "Dialogue
  let converted_text = substitute(converted_text, s_dialogue, '\1\n\t\2', 'g')

  "Continuation Dialogue
  let converted_text = substitute(converted_text, s_continuation_dialogue, '\1\n\t\2', 'g')

  "Dual Dialogue
  let converted_text = substitute(converted_text, s_dual_dialogue, '\1\n\t\2\n\3\n\t\4', 'g')

  "Transition
  let converted_text = substitute(converted_text, s_transition, '\n\0', 'g')

  "Action Description
  let converted_text = substitute(converted_text, s_action_description, '\n\0', 'g')

  "Page Break
  let converted_text = substitute(converted_text, s_page_break, '\n\n\0', 'g')

  "Clean up excessive empty lines
  let converted_text = substitute(converted_text, '\n\n\n\+', '\n\n', 'g')

  "Modify 'converted_text' according to your screenplay format

  "Prompt for title page
  let prompt = input("Do you want to add a title page? (y/n): ")
  if prompt == 'y'
    let title = input("Screenplay Title: ")
    let author = input("Author: ")
    let date = input("Date: ")

    let title_page = printf("Title: %s\nAuthor: %s\nDate: %s\n", title, author, date)
    let converted_text = title_page . "\n\n" . converted_text
  en

  "Replace the buffer contents with the converted text
  call setline(1, split(converted_text, "\n"))

  ec "Your text is converted to screenplay format."

  "Prompt for PDF conversion
  let prompt = input("Do you want to convert the screenplay to PDF? (y/n): ")
  if prompt == 'y'
    let filename = expand('%:r') . '.pdf'
    let papersize = ''
    let geometry = ''
    let papersize_prompt = input("Choose the screenplay's paper size (US Letter/A4): ")
    if papersize_prompt ==# 'US Letter'
      let papersize = 'letter'
      let geometry = '-V geometry:"top=1in, bottom=1in, left=1.25in, right=1.25in"'
    elsei papersize_prompt ==# 'A4'
      let papersize = 'a4'
      let geometry = '-V geometry:"top=2cm, bottom=2cm, left=2.5cm, right=2.5cm"'
    en

    if !empty(papersize)
      let command = 'pandoc --from=markdown --to=pdf ' . geometry . ' -V papersize:' . papersize . ' -o ' . filename
      call system(command)
      ec "Your screenplay is converted to PDF: " . filename
    el
      ec "That is an invalid paper size, the screenplay conversion to PDF is skipped."
    en
    el
      ec "The screenplay conversion to PDF is skipped."
  en
endf
